pragma solidity ^0.4.18;

contract BetOnDice {

    address public adminAddress;
    uint public bettingAmount;
    uint public bettingTimeInSec;
    uint public bettingEnd;
    uint public betCounter;
    uint public totalAmount;
    uint public jackpotAmount;
    uint public numberOfWinners;
    uint public gameRound = 1;

    mapping(uint => address[]) public bets;
    bool public ended = false;
    uint public winNumber;
    mapping(uint => uint) public gameRoundWinningNumber;

    
    function BetOnDice(
        uint _bettingAmount,
        address _adminAddress,
        uint _bettingTimeInSec
    ) public 
    {
        bettingAmount = _bettingAmount;
        adminAddress = _adminAddress;
        bettingTimeInSec = _bettingTimeInSec;
        bettingEnd = now + _bettingTimeInSec;
        betCounter = 0;
        totalAmount = 0;
    }

    modifier asd()
    {
        _;
    }
      function bet(uint betNumber) public payable asd {
        require(!ended);
        require(now < bettingEnd);
        require(msg.value == bettingAmount);
        bets[betNumber].push(msg.sender);
        betCounter += 1;
        totalAmount += msg.value;
    }

    function end() public {
        require(!ended);
        require(now > bettingEnd);
        ended = true;
        uint amount = address (this).balance;
        winNumber = random();
        gameRoundWinningNumber[gameRound] = winNumber;
        if (bets[winNumber].length == 0) {
            resetGame();
            return;
        }
        numberOfWinners = bets[winNumber].length;
        uint profit = amount / numberOfWinners;
        for (uint i = 0; i < numberOfWinners; i++) {
            bets[winNumber][i].transfer(profit);
        }
        resetGame();
    }

    function random() internal returns (uint _winNumber) {
        return block.timestamp % 6 + 1;

        
    }

    function resetGame() internal {
        bettingEnd = now + bettingTimeInSec;
        betCounter = 0;
        totalAmount = address (this).balance;
        gameRound++;
        ended = false;
    }

}